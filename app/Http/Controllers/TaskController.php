<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        $all_task = TRUE;
        return view('tasks.index', compact('tasks' ,'all_task')); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();

        Task::create([
            'title' => $request['title'],
            'status' => 0,
            'user_id' => $user_id,
            'created_at' => date('Y-m-d G:i:s'),
        ]);

        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit',['task'=>$task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result' => 'success','status' => $request->status ), 200);   
        } else {          
            return redirect('tasks');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to delete..");
        } 
        
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('tasks');
    }

    public function done($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to mark as done..");
        }

        $task = Task::findOrFail($id);
        $task->status = 1;
        $task->save();

        return redirect('tasks');
    }

    public function my_tasks()
    {
        $user_id = Auth::id();

        $tasks = Task::where('user_id', $user_id)->get();
        $all_task = FALSE;
        return view('tasks.index', compact('tasks', 'all_task'));
    }
}
