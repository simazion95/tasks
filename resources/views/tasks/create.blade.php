@extends('layouts.app')
@section('content')
<h1>Create a new task</h1>
<form method='post' action="{{action('TaskController@store')}}">
    {{csrf_field()}}
    <div class="form-group">
        <label for ="title_input"> what is your task?</label>
        <input type="text" class= "form-control" name="title" id="title_input">
    </div>
    <div class = "form-group">
        <input type="submit" class="form-control">
    </div>
</form>
@endsection